var gmarkers = [];
var htmls = [];

function bringMeTo(i) {
   gmarkers[i].openInfoWindowHtml(htmls[i]);
}

function mapLoad(){

    var placelinks_html = "";
    var i = 0;
    var map = new GMap2(document.getElementById("map"));
    var placelinks = document.getElementById("placelinks");

  if (GBrowserIsCompatible()) {

    GEvent.addListener(map, "moveend", function() {
      var center = map.getCenter();
      center=center.toString().replace(/\(/," ").replace(/\)/," ");
      document.getElementById("gpscoords").innerHTML = "GPS Coordinates:"+center;
    });
	
    map.addControl(new GLargeMapControl());
    map.addControl(new GMapTypeControl());
    map.addControl(new GScaleControl());

    map.setCenter(new GLatLng(53.3438903986638, -6.251006126403809), 15, G_HYBRID_MAP);

    // Creates an overlay and a marker at the given point with the given label
    function createMarker(name, point, info) {
      var marker = new GMarker(point);
      GEvent.addListener(marker, "click", function() {
        marker.openInfoWindowHtml(info);
      });
      // save the info we need to use later for the sidebar
      gmarkers[i] = marker;
      htmls[i] = info;
      // add a line to the placelinks html
      placelinks_html += '<div class="button" onclick="bringMeTo(' + i + ')">' + name + '</div>';
      i++;
      return marker;
    }
    
    var name_airport = "Dublin Airport";
    var point_airport = new GLatLng(53.42738396726125, -6.246027946472168);
    var text_airport = "<b><a href='http://www.dublinairport.com'>Dublin Airport</a></b><br /><br />For details on transport options between Dublin airport and<br /> Dublin city centre please see <a href='travel.php#arrival'>the aKademy Travel page</a>";
     map.addOverlay(createMarker(name_airport, point_airport, text_airport));

    var name_lloyd = "Lloyd Institute";
    var point_lloyd = new GLatLng(53.34375588860382, -6.251091957092285);
    var text_lloyd = "<b>Lloyd Institute</b><br /><br />- aKademy Information Desk<br />- Contributors Conference<br />- e.V. General Assembly<br />- Small Conference/Press Room<br />- BoF Sessions";
    map.addOverlay(createMarker(name_lloyd, point_lloyd, text_lloyd));

    var name_pchuts = "PC Huts";
    var point_pchuts = new GLatLng(53.34341000364401, -6.251167058944702);
    var text_pchuts = "<b>PC Huts</b><br /><br />- Coding Marathon";
    map.addOverlay(createMarker(name_pchuts, point_pchuts, text_pchuts));

    var name_hostel = "Isaacs Hostel";
    var point_hostel = new GLatLng(53.34998133810203, -6.253441572189331);
    var text_hostel = "<b><a href='http://www.isaacs.ie/isaacs_hostel'>Isaacs Hostel</a></b><br /><br />Official aKademy Accommodation<br />2-5 Frenchmans Lane,<br />Dublin 1.<br />Tel: +353 1 8556215";
    map.addOverlay(createMarker(name_hostel, point_hostel, text_hostel));

    var name_google = "Google HQ Ireland";
    var point_google = new GLatLng(53.339771636315405, -6.236447095870972);
    var text_google = "<b><a href='http://www.google.ie/jobs'>Google HQ Ireland</a></b><br /><br />Google Open House<br />Google is kindly hosting this event,<br />which will be hosted from 6pm on<br /> Monday 25th September for all<br />aKademy participants";
    map.addOverlay(createMarker(name_google, point_google, text_google));

    var name_kennedys = "Kennedys Bar";
    var point_kennedys = new GLatLng(53.34204885502884, -6.250914931297302);
    var text_kennedys = "<b>Kennedys Bar</b><br /><br />If you are around Dublin on Friday 22nd<br />after 6pm, this is where we will be meeting<br />up with other KDE contributors";
    map.addOverlay(createMarker(name_kennedys, point_kennedys, text_kennedys));

    placelinks.innerHTML += placelinks_html;

    var ovcontrol = new GOverviewMapControl(new GSize(200,200));
    map.addControl(ovcontrol);
    setTimeout("ovmap.setMapType(G_NORMAL_MAP);",1);
  }
}
 
